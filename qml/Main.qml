import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import QtQml 2.0
import "UCSComponents"
import QtWebEngine 1.9
import QtQuick.Window 2.2

MainView {
  id:mainview
  objectName: "mainView"
  theme.name: "Ubuntu.Components.Themes.Ambiance"
  applicationName: "deviantart.giampy"
  anchorToKeyboard: true
  automaticOrientation: true
  property bool openExternalUrlInOverlay: true

  width: units.gu(45)
  height: units.gu(75)

    WebEngineView {
      id: webview
      anchors {
        fill: parent
      }
      property var currentWebview: webview
      settings.pluginsEnabled: true
      settings.showScrollBars: false

      profile:  WebEngineProfile {
        id: oxideContext
        httpUserAgent: "Mozilla/5.0 (Linux; Ubuntu 16.04 like Android 4.4) AppleWebKit/537.36 Chrome/77.0.3865.129 Mobile Safari/537.36"
        property alias dataPath: oxideContext.persistentStoragePath
        dataPath: "/.local/share/deviantart.giampy/"
        cachePath: "/.local/share/deviantart.giampy/"
        persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
      }

      zoomFactor: 2.1
      url: "https://www.deviantart.com"
      
    }

    RadialBottomEdge {
      id: nav
      visible: true
      actions: [
        RadialAction {
          id: reload
          iconName: "reload"
          onTriggered: {
            webview.reload()
          }
          text: qsTr("Reload")
        },

        RadialAction {
          id: forward
          enabled: webview.canGoForward
          iconName: "go-next"
          onTriggered: {
            webview.goForward()
          }
          text: qsTr("Forward")
        },

        RadialAction {
          id: home
          iconName: "home"
          onTriggered: {
            webview.url = 'https://www.deviantart.com/'
          }
          text: qsTr("Home")
        },

        RadialAction {
          id: back
          enabled: webview.canGoBack
          iconName: "go-previous"
          onTriggered: {
            webview.goBack()
          }
          text: qsTr("Back")
        }
      ]
    }
    Connections {
      target: Qt.inputMethod
      onVisibleChanged: nav.visible = !nav.visible
    }
}